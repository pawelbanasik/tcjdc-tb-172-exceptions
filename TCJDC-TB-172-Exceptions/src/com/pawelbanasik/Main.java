package com.pawelbanasik;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// int x = 98;
		// int y = 0;
		//
		// System.out.println(divideLBYL(x, y));
		// System.out.println(divideEAFP(x, y));
		// System.out.println(divide(x, y));
		int x = getIntEAFP();
		System.out.println("x is " + x);
	}

	private static int getInt() {
		Scanner s = new Scanner(System.in);
		return s.nextInt();
	}

	private static int getIntLBYL() {
		Scanner s = new Scanner(System.in);
		boolean isValid = true;
		System.out.println("Please enter an integer ");
		String input = s.next();
		
		// sprawdzamy wszystkie znaki wprowadzonego stringa
		for (int i = 0; i < input.length(); i++) {
			if (!Character.isDigit(input.charAt(i))) {
				isValid = false;
				break;
			}
		}
		// jak sa liczbami to parsuje
		if (isValid) {
			return Integer.parseInt(input);

		}
		return 0;
	}

	// prosty i szybki sposob na zrobienie tego co powyzej - mniej kodu
	private static int getIntEAFP() {
		Scanner s = new Scanner(System.in);
		System.out.println("Plese enter an integer ");
		try {
			return s.nextInt();
		} catch (InputMismatchException e) {
			return 0;
		}
		
	}
	
	private static int divideLBYL(int x, int y) {
		if (y != 0) {
			return x / y;
		} else {
			return 0;
		}
	}

	private static int divideEAFP(int x, int y) {
		try {
			return x / y;
		} catch (ArithmeticException e) {
			return 0;
		}
	}

	// zrobil ja zeby pokazac blad jaki bedzie gdy podzieli przez zero
	private static int divide(int x, int y) {
		return x / y;
	}

}
